const express = require('express')
const reqHttp = require('./controllers/httpController')
const app = express()

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,POST');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.get('/', async (req, res) => {
  let apiResponse = await new Promise((resolve, reject) => {
    var Api = {
      host: "https://storage.googleapis.com",
      path: "/cdnbw/test/test.json"
    };
    reqHttp.get(Api.host + Api.path, async (req) => {
      resolve(req.data);
    }, true)
  });

  let toJson = JSON.parse(apiResponse);
  let res_1 = fn_1(toJson);
  let res_2 = fn_2(toJson);
  let res_3 = fn_3(toJson);
  let res_4 = fn_4(toJson);
  res.send({status: 'success', 
    total: res_1, mayorPuntuacion: res_2.nombre + '-' + res_2.puntos,
    menorPuntuacion: res_3.nombre + '-' + res_3.puntos,
    puntuacionMedia: res_4 });

  //res.send({status: 'success', fn1: res_1, fn2: res_2, fn3: res_3, fn4: res_4 });
});

function calculate(_data) {
  // funcion donde se deben calcular los datos solicitados.
  let suma = 0;
  suma = fn_suma(_data);

  // suma = _data.reduce(function(data){
  //   return suma + data.puntos;
  // }, 0);
  return suma;

  // var linq = Enumerable.From(_data);
  // suma = linq.sum('puntos').select();
  // console.log(suma);
}

//2.- Calcular la sumatoria de todos los puntos cargados (total).
function fn_1(_data){
  let total = 0;
  for (var i = 0; i < _data.movimientos.length; i++) {
    total+= _data.movimientos[i].puntos;
  }
  return total;
}
//3.- Retornar el máximo valor de Puntos encontrados, junto al nombre de la persona que lo recibió.
function fn_2(_data){
  let retorno = {};
  var maxValue = Number.MIN_VALUE; 
  var indice = 0;
  for(var i=0;i<_data.movimientos.length;i++){ 
      if(_data.movimientos[i].puntos > maxValue){
        maxValue = _data.movimientos[i].puntos;
        indice=i;
      }
  }
  return _data.movimientos[indice];
}
//4.- Retornar el mínimo valor de Puntos encontrados, junto al nombre de la persona que los recibió.
function fn_3(_data){
  var minValue = Number.MAX_VALUE; 
  var indice = 0;
  for(var i=0;i<_data.movimientos.length;i++){ 
      if(_data.movimientos[i].puntos < minValue){
        minValue = _data.movimientos[i].puntos;
        indice=i;
      }
  }
  return _data.movimientos[indice];
}
//5.- Calcular el valor medio de los puntos entregados.
function fn_4(_data){
  var suma = fn_1(_data);
  return suma/_data.movimientos.length;
}

app.listen(3000, () => {
  console.log('server started');
}); 