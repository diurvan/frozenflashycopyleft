'use strict';
const http = require("http"),
	https = require("https"),
	request = require("request");

const HttpRequest = new function() {
  this.get = function(_url, _callback){
    let options = {
      url: _url,
      method: 'GET'
	};
    request(options, function(err, response, body) {

        let data = {'status':'success', 'message': 'Request ejecutado sin problemas.','data':body};

      	_callback(data);
    });
  };

	this.post = function (_url,_headers,_param,isJsonData=false,_callback)
	{
		let options = {
			url: _url,
			method: 'POST',
			headers: _headers
		};
		if(isJsonData)
		{
			options.json = _param;
		}
		else
		{
			options.form = _param;
		}
		request(options, function(err, response, body){
			if(!err && response.statusCode=== 200)
			{
				_callback({'status':'success', 'message': 'Request ejecutado sin problemas.','data':body});
			}
			else
			{
				_callback({'status':'error', 'message': err, 'data' : body})
			}
		});

	};
};

module.exports = HttpRequest;