Este pequeño script llama a un endPoint .json información de puntajes asignados a usuarios de Dcanje.com. 

la URL del endPoint es https://storage.googleapis.com/cdnbw/test/test.json 

Los puntos a revisar son los siguientes:

1.- Solucionar los errores presentados para que el script funcione.
2.- Calcular la sumatoria de todos los puntos cargados (total).
3.- Retornar el máximo valor de Puntos encontrados, junto al nombre de la persona que lo recibió.
4.- Retornar el mínimo valor de Puntos encontrados, junto al nombre de la persona que los recibió.
5.- Calcular el valor medio de los puntos entregados.

Hints:
- se considerarán como mejoras la optimizacióón de código, como por ejemplo utilización de funciones recursivas y reducción de código innecesario.

- El resultado mostrado por navegador debe ser similar a :

{
  status: "success",
  total: 999999,
  mayorPuntuacion: "Luis - 100000",
  menorPuntuacion: "José - 1200",
  puntuacionMedia: 56000
}